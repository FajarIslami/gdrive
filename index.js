const express = require('express');
const dotenv = require('dotenv');
const colors = require('colors');
const morgan = require('morgan');
const cors = require('cors');

dotenv.config();
const app = express();
app.use(cors());
app.use(morgan('dev'));

// const TOKEN_PATH = 'token.json';
const PORT = process.env.PORT || 200;
const gdriveRoutes = require('./routes/gdriveRoutes');

app.use(express.json());
app.get('/', (req, res) => {
  res.send('Node Integrated to GDrive');
});

app.use('/file', gdriveRoutes);

app.listen(PORT, () => {
  console.log(`Server running on port ${PORT}`.yellow.bold);
});
