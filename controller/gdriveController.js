var async = require('async');
const fs = require('fs');
const { google } = require('googleapis');
const { authorize } = require('../middleware/OAuth2');
const colors = require('colors');

module.exports.getAllFiles = (req, res) => {
  fs.readFile('credentials.json', (err, content) => {
    if (err) return console.log('Error loading client secret file:', err);
    authorize(JSON.parse(content), (auth) => {
      const drive = google.drive({ version: 'v3', auth });

      drive.files.list(
        {
          pageSize: 10,
          fields: 'nextPageToken, files(id, name)',
        },
        (err, resGdrive) => {
          if (err) return console.log('The API returned an error: ' + err);
          // console.log('res:', res.data);
          const files = resGdrive.data.files;
          if (files.length) {
            console.log('Files:');
            files.map((file) => {
              console.log(`${file.name} (${file.id})`);
            });
            // console.log(files);
            res.send(files);
          } else {
            console.log('No files found.');
          }
        },
      );
    });
  });
};

module.exports.newFolder = (req, res) => {
  const { folder } = req.body;
  fs.readFile('credentials.json', (err, content) => {
    if (err) return console.log('Error loading client secret file:', err);
    authorize(JSON.parse(content), (auth) => {
      const drive = google.drive({ version: 'v3', auth });
      var fileMetadata = {
        name: folder,
        mimeType: 'application/vnd.google-apps.folder',
      };
      drive.files.create(
        {
          resource: fileMetadata,
          fields: 'id',
        },
        function (err, file) {
          if (err) {
            // Handle error
            console.error(err);
          } else {
            // console.log('file:', file.config.data.name);
            // console.log('Folder Id: ', file.data.id);
            res.status(201).send({
              msg: 'FIle berhasil dibuat',
              namaFolder: file.config.data.name,
              idFolder: file.data.id,
            });
          }
        },
      );
    });
  });
};

module.exports.findFolder = (req, res) => {
  // const { folder } = req.body;
  const params = req.params.name;
  fs.readFile('credentials.json', (err, content) => {
    if (err) return console.log('Error loading client secret file:', err);
    authorize(JSON.parse(content), (auth) => {
      const drive = google.drive({ version: 'v3', auth });
      var pageToken = null;
      // Using the NPM module 'async'
      async.doWhilst(
        function (callback) {
          drive.files.list(
            {
              q: `fullText contains '${params}'`,
              // q: `'1ctpjhihumFQGD8p6lkoN9lnh2XR57Xqr' in parents`,
              fields: 'nextPageToken, files(id, name)',
              spaces: 'drive',
              pageToken: pageToken,
            },
            function (err, resGdrive) {
              if (err) {
                // Handle error
                console.error('250:', err);
                callback(err);
              } else {
                // console.log(params);
                // console.log(resGdrive);
                resGdrive.data.files.forEach(function (file) {
                  console.log('Found file: ', file.name, 'id: ', file.id);
                });
                pageToken = resGdrive.nextPageToken;
                res.send(resGdrive.data.files);
                callback();
              }
            },
          );
        },
        function () {
          return !!pageToken;
        },
        function (err) {
          if (err) {
            // Handle error
            console.error('268:', err);
          } else {
            // All pages fetched
          }
        },
      );
    });
  });
};

module.exports.listFolderById = (req, res) => {
  const params = req.params.id;
  fs.readFile('credentials.json', (err, content) => {
    if (err) return console.log('Error loading client secret file:', err);
    authorize(JSON.parse(content), (auth) => {
      const drive = google.drive({ version: 'v3', auth });
      var pageToken = null;
      // Using the NPM module 'async'
      async.doWhilst(
        function (callback) {
          drive.files.list(
            {
              q: `'${params}' in parents`,
              fields: 'nextPageToken, files(id, name)',
              spaces: 'drive',
              pageToken: pageToken,
            },
            function (err, resGdrive) {
              if (err) {
                // Handle error
                console.error('250:', err);
                callback(err);
              } else {
                // console.log(params);
                // console.log(resGdrive);
                // console.log(resGdrive.data.files.length);
                if (resGdrive.data.files.length < 1) {
                  res.send('folder kosong');
                } else {
                  resGdrive.data.files.forEach(function (file) {
                    console.log('Found file: ', file.name, 'id: ', file.id);
                  });
                  pageToken = resGdrive.nextPageToken;
                  res.send(resGdrive.data.files);
                }
                callback();
              }
            },
          );
        },
        function () {
          return !!pageToken;
        },
        function (err) {
          if (err) {
            // Handle error
            console.error('268:', err);
          } else {
            // All pages fetched
          }
        },
      );
    });
  });
};

module.exports.createChildFolder = (req, res) => {
  const params = req.params.id;
  const { folder } = req.body;
  fs.readFile('credentials.json', (err, content) => {
    if (err) return console.log('Error loading client secret file:', err);
    authorize(JSON.parse(content), (auth) => {
      const drive = google.drive({ version: 'v3', auth });

      var folderId = params; // ini diubah

      var fileMetadata = {
        name: folder,
        parents: [folderId],
        mimeType: 'application/vnd.google-apps.folder',
      };

      console.log('Create');
      drive.files.create(
        {
          resource: fileMetadata,
          fields: 'id',
        },
        function (err, file) {
          if (err) {
            // Handle error
            console.error(err);
          } else {
            console.log('Folder Id: ', file.data.id);
            res.status(201).json({
              msg: 'Berhasil buat folder',
              namaFolder: file.config.data.name,
              idFolder: file.data.id,
            });
          }
        },
      );
    });
  });
};

module.exports.uploadFile = (req, res) => {
  const params = req.params.id;
  fs.readFile('credentials.json', (err, content) => {
    if (err) return console.log('Error loading client secret file:', err);
    authorize(JSON.parse(content), (auth) => {
      const drive = google.drive({ version: 'v3', auth });

      var folderId = params; // ini diubah
      // var folderId = '19h7LUfYZ9P0BzMZTWsy-m_r5jb2B4BVP'; // ini diubah

      // File yang diupload bentuk foto
      var fileMetadata = {
        name: 'tukui.jpg',
        parents: [folderId],
      };
      var media = {
        mimeType: 'image/jpeg',
        body: fs.createReadStream('files/tsuki.jpg'),
      };

      console.log(params);
      drive.files.create(
        {
          resource: fileMetadata,
          media: media,
          fields: 'id',
        },
        function (err, file) {
          console.log('upload');
          if (err) {
            // Handle error
            console.error(err);
          } else {
            console.log('File Id: ', file.data.id);
            res.status(201).json({
              msg: 'Berhasil upload File',
              namaFile: file.config.data.name,
              idFile: file.data.id,
            });
          }
        },
      );
    });
  });
};

module.exports.addPermissions = (req, res) => {
  const params = req.params.id;
  const { type, role, emailAddress } = req.body;
  fs.readFile('credentials.json', (err, content) => {
    if (err) return console.log('Error loading client secret file:', err);
    authorize(JSON.parse(content), (auth) => {
      const drive = google.drive({ version: 'v3', auth });
      var fileId = `${params}`; // ini diubah
      // var fileId = '19h7LUfYZ9P0BzMZTWsy-m_r5jb2B4BVP'; // ini diubah
      var permissions = [
        {
          // type: 'user',
          // role: 'writer',
          // emailAddress: 'tjakrabirawa65@gmail',
          type,
          role,
          emailAddress,
        },
      ];
      console.log(params);
      // Using the NPM module 'async'
      async.eachSeries(
        permissions,
        function (permission, permissionCallback) {
          drive.permissions.create(
            {
              resource: permission,
              fileId: fileId,
              fields: 'id',
            },
            function (err, resGdrive) {
              console.log('req.params:', req.params);
              if (err) {
                // Handle error...
                console.error('237:', err);
                console.log('238:', params);
                permissionCallback(err);
              } else {
                console.log(resGdrive);
                console.log('Permission ID: ', resGdrive.data.id);
                res.status(200).json({
                  msg: `Berhasil menambah ${resGdrive.config.data.emailAddress} ke folder id ${params}`,
                });
                permissionCallback();
              }
            },
          );
        },
        function (err) {
          if (err) {
            // Handle error
            console.log('254:', params.red);
            console.error(err);
            res.send(err);
          } else {
            // All permissions inserted
          }
        },
      );
    });
  });
};
