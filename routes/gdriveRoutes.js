const express = require('express');
const router = express.Router();
const gdriveController = require('../controller/gdriveController');

router.get('/', gdriveController.getAllFiles);
router.get('/listFolderById/:id', gdriveController.listFolderById);
router.get('/:name', gdriveController.findFolder);
router.post('/', gdriveController.newFolder);
router.post('/:id/upload', gdriveController.uploadFile);
router.post('/:id/permissions', gdriveController.addPermissions);
// router.post('/permissions/:id', gdriveController.addPermissions);
router.post('/:id', gdriveController.createChildFolder);

module.exports = router;
